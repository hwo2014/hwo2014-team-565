class Driver(object):
    """ Cleverly schedules lane switch requests so that bot code can spam
    requests as much as it wants but the server only sees the meaningful
    requests. 
    """

    def __init__(self, car, send_raw_message):
        self.car = car
        self.send_raw_message = send_raw_message

        self.throttle_value = 0.
        self.desired_switch = None
        self.sent_switch = None

    def throttle(self, throttle_value):
        self.throttle_value = throttle_value
    
    def switch(self, direction):
        self.desired_switch = direction

    def send_message(self):
        if len(self.car.is_switching) >= 2 and self.car.is_switching[-2] == False and self.car.is_switching[-1] == True:
            self.desired_switch = None
            self.sent_switch = None

        if self.desired_switch != self.sent_switch:
            self.send_raw_message('switchLane', self.desired_switch)
            self.sent_switch = self.desired_switch
        else:
            self.send_raw_message('throttle', self.throttle_value)

