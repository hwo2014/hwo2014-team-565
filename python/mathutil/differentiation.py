from __future__ import division

def derivative(y, x):
    """Given lists of y and x coordinates, calculates the derivative at the last element.
    
    Returns None if there are not enough samples to calculate the derivative."""

    assert(len(y) == len(x))
    
    if len(y) < 2:
        return None

    yf = y[-1]
    yi = y[-2]
    xf = x[-1]
    xi = x[-2]

    if yf == None or yi == None or xf == None or xi == None:
        return None

    return float(yf - yi) / float(xf - xi)
