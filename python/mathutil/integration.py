from __future__ import division
from math import sqrt, pow

# TODO: If we find a way to include numpy, this can be replaced with a proper numerical integrator.
# From http://en.wikipedia.org/wiki/Simpson%27s_rule 
def integrate(f, a, b, n):
    """Approximates the definite integral of f from a to b by
    the composite Simpson's rule, using n subintervals"""

    n -= n % 2 # n must be even

    h = (b - a) / n
    s = f(a) + f(b)
 
    for i in range(1, n, 2):
        s += 4 * f(a + i * h)
    for i in range(2, n-1, 2):
        s += 2 * f(a + i * h)
 
    return s * h / 3

def polar_arclength(r, drdtheta, theta0, theta1, num_samples):
    ds = lambda theta: sqrt(pow(r(theta), 2.) + pow(drdtheta(theta), 2.))
    return integrate(ds, theta0, theta1, num_samples)

def linear_radius_transition_length(r1, r2, angle, num_samples):
    r = lambda theta: r1 + (r2 - r1) * theta / angle
    drdtheta = lambda theta: (r2 - r1) / angle
    return polar_arclength(r, drdtheta, 0., angle, num_samples)
