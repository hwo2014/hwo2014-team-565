import pickle
from pprint import pprint
import argparse

from data.keimola import track
from model.track2 import TrackCoord, CURVED_PIECE

parser = argparse.ArgumentParser()
parser.add_argument('filename')
args = parser.parse_args()

data = pickle.load(open(args.filename))

def car_position(x):
    return x['car_position']

def piece_position(x):
    return car_position(x)['piecePosition']

def lane(x):
    return piece_position(x)['lane']

def end_lane_index(x):
    return lane(x)['endLaneIndex']

def start_lane_index(x):
    return lane(x)['startLaneIndex']

def piece_index(x):
    return piece_position(x)['pieceIndex']

def in_piece_distance(x):
    return piece_position(x)['inPieceDistance']

# switch_idxs = [idx for (idx, x) in enumerate(data) if 'car_position' in x and start_lane_index(x) != end_lane_index(x)]
#
# consecutive_idxs = []
# for i in range(0, len(switch_idxs)):
#     if i == 0 or switch_idxs[i-1] != switch_idxs[i] - 1:
#         consecutive_idxs.append([])
#     consecutive_idxs[-1].append(switch_idxs[i])
# 
# print consecutive_idxs
# 
# for idxs in consecutive_idxs:
#     switch_blobs = [data[i] for i in xrange(min(idxs) - 10, max(idxs) + 10)]
# 
#     print '=========='
#     for x in switch_blobs:
#         print '%d %f %f %d' % (x['tick'], x['speed'], in_piece_distance(x), piece_index(x))

# consecutive_idxs = []
# for i in range(0, len(curve_idxs)):
#     if i == 0 or curve_idxs[i-1] != curve_idxs[i] - 1:
#         consecutive_idxs.append([])
#     consecutive_idxs[-1].append(curve_idxs[i])
# 
# for idxs in consecutive_idxs:
#     curve_blobs = [data[i] for i in xrange(min(idxs) - 10, max(idxs) + 10)]
# 
#     print '========'
#     for x in curve_blobs:
#         print '%d,%f,%f,%f,%f,%f' % (piece_index(x), x['distance'], x['speed'], x['deflection'], x['angular_speed'], x['angular_acceleration'])
# 
# for x in data:
#     if 'car_position' in x:
#         try:
#             print '%d,%f,%f,%f,%f,%f' % (piece_index(x), x['distance'], x['speed'], x['deflection'], x['angular_speed'], x['angular_acceleration'])
#         except:
#             pass

def extract_from_pieces(data, piece_idxs):
    relevant_idxs = [idx for (idx, x) in enumerate(data) if 'car_position' in x and x['deflection'] != None and x['angular_speed'] != None and x['angular_acceleration'] != None and piece_index(x) in piece_idxs]

    consecutive_idxs = []
    for i in range(0, len(relevant_idxs)):
        if i == 0 or relevant_idxs[i-1] != relevant_idxs[i] - 1:
            consecutive_idxs.append([])
        consecutive_idxs[-1].append(relevant_idxs[i])

    consecutive_blobs = []
    for idxs in consecutive_idxs:
        consecutive_blobs.append([data[i] for i in idxs])

    return consecutive_blobs

import numpy as np
from simulate import torque
from scipy.optimize import minimize


iii = range(35) 
efp = extract_from_pieces(data, iii)
trainblobs = efp[0] + efp[1] + efp[2]
testblobs = efp[0] + efp[1] + efp[2]

def obj(x):
    F1 = x[0]
    F2 = x[1]
    d1 = 10.
    d2 = 30.
    r = 0.
    alpha = x[2]
    radpow = x[3]
    vpow = x[4]
    for point in trainblobs:
        beta = 0
        lane_piece = track.get_lane_piece((TrackCoord(
            point['car_position']['piecePosition']['pieceIndex'],
            0,
            0,
            0.)))
        if lane_piece.piece_type() == CURVED_PIECE:
            radius = pow(lane_piece.initial_radius(), radpow)
            if lane_piece.track_piece.angle > 0:
                beta = alpha / radius
            else:
                beta = -alpha / radius

        v = point['speed']

        r += pow(torque(F1, F2, d1, d2, v, point['deflection'], point['angular_speed']) + pow(v, vpow) * beta - point['angular_acceleration'], 2.)
    print r
    return r

result = minimize(obj, np.array([1., 1., 0., 2., 2.]))

print result

from scipy.integrate import odeint
from numpy import arange
import numpy as np

# Differential equation

# theta0 = testblobs[0]['deflection'] 
# omega0 = testblobs[0]['angular_speed']
# 
# result_x = result.x
# def pt(a, b):
#     d1 = 10.
#     d2 = 30.
#     v = 6.5
#     return torque(result_x[0], result_x[1], d1, d2, v, a, b) + result_x[2]
# 
# def f(y, t):
#     (theta, omega) = y
#     return (omega, pt(theta, omega))
# 
# ts = arange(0, 100, 1)
# soln = odeint(f, (theta0, omega0), ts)
# 
# soln[:, 0] = soln[:, 0] / 20.
# 
# #accels = np.array([[pt(x[0], x[1])] for x in soln])
# #soln = np.concatenate((soln, accels), axis=1)
# 
# from pylab import show, plot 
# 
# plot(ts, soln)
# plot(range(len(testblobs)), [x['deflection']/20. for x in testblobs])
# plot(range(len(testblobs)), [x['angular_speed'] for x in testblobs])
# #plot(range(len(testblobs)), [x['angular_acceleration'] for x in testblobs])
# show()
