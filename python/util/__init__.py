import json


def _flatten_dict(d, prefix=''):
    if isinstance(d, dict):
        if len(prefix) > 0:
            prefix = prefix + '.'
        flattened = {}
        for key, value in d.items():
            if isinstance(value, dict):
                flattened.update(_flatten_dict(value, prefix + key))
            else:
                flattened[prefix + key] = value
        return flattened
    else:
        return d


def _make_dict(obj):
    if hasattr(obj, '_asdict'):  # `namedtuple`s
        return {
            key: _make_dict(value)
            for key, value in obj._asdict().items()
        }
    elif isinstance(obj, dict):
        return {
            key: _make_dict(value)
            for key, value in obj.items()
        }
    else:
        return obj
