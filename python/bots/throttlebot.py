from logbot import LogBot


class ThrottleBot(LogBot):

    def __init__(self, socket, name, key, throttleFn):
        self.throttleFn = throttleFn
        super(ThrottleBot, self).__init__(socket, name, key)

    def throttle(self, throttle):
        self.last_throttle = throttle
        self.msg("throttle", throttle)

    def on_car_positions(self, msg):
        throttle = self.throttleFn(self.car, msg)
        self.throttle(throttle)
        self.car.on_car_positions(msg)
