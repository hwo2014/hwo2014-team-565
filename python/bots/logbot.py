import datetime
import json
import cPickle as pickle
from model.car import Car
from model.track2 import _make_track
from drivers.driver import Driver


class LogBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.last_throttle = None
        self.car = None
        self.car_id = None
        self.driver = None
        self.track2 = None
        self.logs = []

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def log(self):
        if self.car is None:
            return
        row = self.car.get_latest_state()
        if "crash" not in row:
            row["crash"] = True
        row["throttle"] = self.last_throttle
        self.logs.append(row)

    def write_logs(self):
        with open("log/logfile_%s.log" % datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S"), "w") as f:
            pickle.dump(self.logs, f, protocol=1)

    def throttle(self, throttle):
        self.last_throttle = throttle
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, msg):
        print("Joined")
        self.ping()

    def on_car_init(self, msg):
        data = msg["data"]
        self.car_id = data

    def on_game_init(self, msg):
        data = msg["data"]
        self.track2 = _make_track(data['race']['track'])
        self.car = Car(self.car_id, self.track2)
        self.driver = Driver(self.car, self.msg)

    def on_game_start(self, msg):
        print("Race started")
        self.ping()

    def on_game_end(self, msg):
        print("Race ended")
        self.ping()

    def on_error(self, msg):
        data = msg["data"]
        print("Error: {0}".format(data))
        self.ping()

    def on_car_positions(self, msg):
        self.car.on_car_positions(msg)
        self.driver.send_message()

    def on_crash(self, msg):
        print("Someone crashed")
        self.car.on_crash(msg)
        self.ping()

    def on_spawn(self, msg):
        print("Someone spawned")
        self.car.on_spawn(msg)
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'yourCar': self.on_car_init,
            'gameInit': self.on_game_init
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](msg)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            self.log()
            line = socket_file.readline()
        self.write_logs()
