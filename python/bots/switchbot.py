from bots.logbot import LogBot


class SwitchBot(LogBot):

    def on_car_positions(self, *args, **kwargs):
        super(SwitchBot, self).on_car_positions(*args, **kwargs)

        coord = self.car.coords[-1]

        self.driver.throttle(0.3)

        current_lane = coord.end_lane_idx
        other_lane = 1 - current_lane 
        (other_lane_direction, other_lane_distance) = self.track2.lane_offset(current_lane, other_lane)
        self.driver.switch(other_lane_direction)
