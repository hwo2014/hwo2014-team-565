import socket
import sys

from bots.noob import NoobBot
from bots.logbot import LogBot
from bots.switchbot import SwitchBot


def run_bot(Bot, host, port, name, key):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))
    bot = Bot(s, name, key)
    bot.run()


if __name__ == "__main__":
    if len(sys.argv) != 6:
        print("Usage: ./run host port bot_type botname botkey")
    else:
        host, port, bot_type, name, key = sys.argv[1:6]
        BotType = globals()[bot_type]
        print("Connecting with parameters:")
        print("host={0}, port={1}, BotType = {2}, bot name={3}, key={4}".format(*sys.argv[1:6]))
        run_bot(BotType, host, port, name, key)
