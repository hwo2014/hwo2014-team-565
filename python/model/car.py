from collections import deque
from math import radians

from mathutil.differentiation import derivative
from track2 import _make_track
from track2 import _make_coord


# How much history to store. This should be >= 2 for derivatives to work.
HISTORY_LENGTH = 5


class Car(object):
    """Car encapsulates a car.

    To construct, pass in a car id as specified in the HWO docs and pass in a
    Track model.

    To use, pass server messages of type carPositions, crash, and spawn to
    on_car_positions, on_crash, on_spawn respectively.

    To read the current state, look at the last element of each of the instance
    variables initialized in _init_histories. When the car is crashed, the
    histories will be empty. When there is not enough information to calculate
    a derivative, its value will be None.
    
    To see if the car is crashed, check the currently_crashed instance variable.
    """

    def __init__(self, id, track2):
        self.id = id
        self.track2 = track2

        self.currently_crashed = False
        self._current_distance = 0
        self._init_histories()

    def _init_histories(self):
        # All the following lists are "histories". Index -1 refers to the
        # current state, -2 refers to the previous state, etc. We store
        # HISTORY_LENGTH previous values.

        # history of car positions
        self.car_positions = self._make_history_container()

        # history of coordinates
        self.coords = self._make_history_container()

        # history of game_ticks
        self.game_ticks = self._make_history_container()

        # history of whether we are switching
        self.is_switching = self._make_history_container()

        # 0-th, 1-st, 2-nd order derivatives
        # distance is total distance traveled, NOT position within the track!
        # distance should not have any discontinuities
        # (it might have discontinuities during lane switches, this should be fixed eventually)
        self.distance_0 = self._make_history_container()
        self.distance_1 = self._make_history_container()
        self.distance_2 = self._make_history_container()

        # 0-th, 1-st, 2-nd order derivatives
        # car deflection angle
        self.angle_radians_0 = self._make_history_container()
        self.angle_radians_1 = self._make_history_container()
        self.angle_radians_2 = self._make_history_container()

    def _make_history_container(self):
        return deque(maxlen = HISTORY_LENGTH) 

    def on_car_positions(self, msg):
        if self.currently_crashed:
            return

        data = msg['data']
        game_tick = msg['gameTick'] if 'gameTick' in msg else 0

        self.game_ticks.append(game_tick)

        car_position = [p for p in data if p['id'] == self.id][0]
        self.car_positions.append(car_position)

        coord = _make_coord(car_position['piecePosition'])
        self.coords.append(coord)

        is_switching = coord.start_lane_idx != coord.end_lane_idx
        self.is_switching.append(is_switching)

        if len(self.car_positions) > 1:
            coord0 = self.coords[-2] 
            coord1 = self.coords[-1]
            delta_distance = self.track2.distance(coord0, coord1)
            self._current_distance += delta_distance

        self.distance_0.append(self._current_distance)
        self.distance_1.append(derivative(self.distance_0, self.game_ticks))
        self.distance_2.append(derivative(self.distance_1, self.game_ticks))

        angle_degrees = car_position['angle']
        angle_radians = radians(angle_degrees)
        self.angle_radians_0.append(angle_radians)
        self.angle_radians_1.append(derivative(self.angle_radians_0, self.game_ticks))
        self.angle_radians_2.append(derivative(self.angle_radians_1, self.game_ticks))

    def get_latest_state(self):
        if len(self.game_ticks) == 0:
            return {}
        return {
            "tick": self.game_ticks[-1],
            "car_position": self.car_positions[-1],
            "distance": self.distance_0[-1],
            "speed": self.distance_1[-1],
            "acceleration": self.distance_2[-1],
            "deflection": self.angle_radians_0[-1],
            "angular_speed": self.angle_radians_1[-1],
            "angular_acceleration": self.angle_radians_2[-1],
            "crash": self.currently_crashed
        }

    def on_crash(self, msg):
        if msg['data'] == self.id:
            self._init_histories()
            self.currently_crashed = True 

    def on_spawn(self, msg):
        if msg['data'] == self.id:
            self.currently_crashed = False
