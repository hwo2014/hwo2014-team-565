import itertools
from math import radians, sqrt, pow, cos, copysign

from collections import namedtuple
from mathutil.integration import linear_radius_transition_length


# Piece types
STRAIGHT_PIECE = 0
CURVED_PIECE = 1


StraightTrackPiece = namedtuple(
    'StraightTrackPiece',
    [
        'piece_type',
        'switch',
        'length',
    ],
)


CurvedTrackPiece = namedtuple(
    'CurvedTrackPiece',
    [
        'piece_type',
        'switch',
        'radius',
        'angle',
    ],
)


Lane = namedtuple(
    'Lane',
    [
        'index',
        'distance_from_center',
    ],
)


class LanePiece(object):

    def __init__(self, track_piece, lane0, lane1):
        self.track_piece = track_piece
        self.lane0 = lane0
        self.lane1 = lane1

    def piece_type(self):
        return self.track_piece.piece_type

    def initial_radius(self):
        assert(self.piece_type() == CURVED_PIECE)
        if self.track_piece.angle < 0:
            return self.track_piece.radius + self.lane0.distance_from_center
        elif self.track_piece.angle > 0:
            return self.track_piece.radius - self.lane0.distance_from_center

    def final_radius(self):
        assert(self.piece_type() == CURVED_PIECE)
        if self.track_piece.angle < 0:
            return self.track_piece.radius + self.lane1.distance_from_center
        elif self.track_piece.angle > 0:
            return self.track_piece.radius - self.lane1.distance_from_center

    def initial_inv_radius(self):
        if self.piece_type() == CURVED_PIECE:
            return copysign(1. / self.initial_radius(), self.track_piece.angle)
        else:
            return 0.

    def final_inv_radius(self):
        if self.piece_type() == CURVED_PIECE:
            return copysign(1. / self.final_radius(), self.track_piece.angle)
        else:
            return 0.

    def piece_length(self):
        if self.track_piece.piece_type == STRAIGHT_PIECE:
            # Calculate piece length as the distance from the start of the
            # initial lane to the end of the final lane. This does not exactly
            # match how the track works, but it gets very close.

            lanes_offset = self.lane0.distance_from_center - self.lane1.distance_from_center
            return sqrt(pow(self.track_piece.length, 2.) + pow(lanes_offset, 2.))

        elif self.track_piece.piece_type == CURVED_PIECE:
            radius0 = self.initial_radius()
            radius1 = self.final_radius()

            if radius0 == radius1: 
                # If we don't change radius, we can calculate this much faster.
                return radius0 * abs(self.track_piece.angle)
            else:
                # This does not match exactly hw the track works, but it gets very close.
                return linear_radius_transition_length(radius0, radius1, abs(self.track_piece.angle), 4)

TrackCoord = namedtuple(
    'TrackCoord',
    [
        'track_piece_idx',
        'start_lane_idx',
        'end_lane_idx',
        'length_offset',
    ]
)


def _make_track_piece(obj):
    switch = obj.get('switch') == True
    if 'length' in obj:
        return StraightTrackPiece(
            STRAIGHT_PIECE,
            switch,
            obj['length'],
        )
    elif 'angle' in obj:
        return CurvedTrackPiece(
            CURVED_PIECE,
            switch,
            obj['radius'],
            radians(obj['angle']),
        )
    else:
        raise Exception('Unrecognized track piece obj: %s' % obj)


def _make_lane(obj):
    return Lane(obj['index'], obj['distanceFromCenter'])


def _make_track(obj):
    return Track(
        obj['id'],
        obj['name'],
        map(_make_track_piece, obj['pieces']),
        map(_make_lane, obj['lanes'])
    )


def _make_coord(obj):
    return TrackCoord(
        obj['pieceIndex'],
        obj['lane']['startLaneIndex'],
        obj['lane']['endLaneIndex'],
        obj['inPieceDistance'],
    )


class Track(object):

    def __init__(self, track_id, name, track_pieces, lanes):
        self.track_id = track_id
        self.name = name
        self.track_pieces = track_pieces
        self.lanes = {lane.index: lane for lane in lanes} # Docs do not guarentee that lanes come in index order.

        left_to_right_sorted_lanes = sorted(lanes, key=lambda lane: lane.distance_from_center)

        # Dictionary mapping lane.index -> left to right order (0 is leftmost).
        # Docs do not guarentee that lanes come in left to right order.
        self.lane_left_to_right_order = \
            {lane.index: lane_order for (lane_order, lane) in enumerate(left_to_right_sorted_lanes)}

        # Dictionary mapping left to right order (0 is leftmost) -> lane.index
        self.lane_index_by_left_to_right_order = \
            {lane_order: lane.index for (lane_order, lane) in enumerate(left_to_right_sorted_lanes)}

    def distance(self, coord0, coord1):
        if coord0.track_piece_idx == coord1.track_piece_idx:
            return coord1.length_offset - coord0.length_offset

        initial_lane_piece = LanePiece(
            self.track_pieces[coord0.track_piece_idx],
            self.lanes[coord0.start_lane_idx],
            self.lanes[coord0.end_lane_idx],
        )
        d = initial_lane_piece.piece_length() - coord0.length_offset

        if coord1.track_piece_idx < coord0.track_piece_idx:  # Wrapped around
            indices = itertools.chain(
                xrange(coord0.track_piece_idx + 1, len(self.track_pieces)),
                xrange(coord1.track_piece_idx)
            )
        else:
            indices = xrange(coord0.track_piece_idx + 1, coord1.track_piece_idx)

        for idx in indices:
            lane_piece = LanePiece(
                self.track_pieces[idx],
                self.lanes[coord0.end_lane_idx],
                self.lanes[coord0.end_lane_idx],
            )
            d += lane_piece.piece_length()

        d += coord1.length_offset

        return d

    # Returns the direction and number of lanes necessary to switch from lane0 to lane1.
    def lane_offset(self, lane0_index, lane1_index):
        lane0_order = self.lane_left_to_right_order[lane0_index]
        lane1_order = self.lane_left_to_right_order[lane1_index]
        delta_order = lane1_order - lane0_order
        if delta_order > 0:
            return ('Right', abs(delta_order))
        else:
            return ('Left', abs(delta_order))

    # Returns the lane_index of the lane that is num_lanes in direction from lane0.
    # Raises exception if the move goes off the edge of the track.
    def displace_lane_index(self, lane0_index, direction, num_lanes):
        lane0_order = self.lane_left_to_right_order[lane0_index]
        if direction == 'Left':
            lane1_order = lane0_order - num_lanes
        elif direction == 'Right':
            lane1_order = lane0_order + num_lanes
        else:
            raise Exception('Unrecognized direction')
        return self.lane_index_by_left_to_right_order[lane1_order]

    def get_lane_piece(self, coord):
        return LanePiece(
            self.track_pieces[coord.track_piece_idx],
            self.lanes[coord.start_lane_idx],
            self.lanes[coord.end_lane_idx]
        )

    def beginning_of_next_piece(self, coord):
        return TrackCoord(
            (coord.track_piece_idx + 1) % len(self.track_pieces),
            coord.end_lane_idx,
            coord.end_lane_idx,
            0.
        )
