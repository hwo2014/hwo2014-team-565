from __future__ import division
import time
from collections import namedtuple
from math import sin, exp, copysign, cos, sqrt
import numpy as np
from scipy.integrate import ode

PhysicsModelParameters = namedtuple(
    'PhysicsModelParameters',
    [
        'f_lin',
        'f_rot',
        'alpha_cent',
        'threshold',
        'power',
        'drag'
    ]
)

PhysicsModelState = namedtuple(
    'PhysicsModelState',
    [
        'coord',
        'v',
        'theta',
        'omega'
    ]
)

def clamp(v, m, M):
    return max(m, min(M, v))

class PhysicsModel(object):
    def __init__(self, track):
        self.track = track

    def angular_acceleration(self, params, v, theta, omega, inv_radius): 
        lin_torque = params.f_lin * sin(theta)
        rot_torque = params.f_rot * omega

        cent_torque = params.alpha_cent * v * v * inv_radius 
        abs_thresh_cent_torque = max(0., abs(cent_torque) - params.threshold)
        thresh_cent_torque = copysign(abs_thresh_cent_torque, cent_torque)
       
        return lin_torque + rot_torque + thresh_cent_torque

    def velocity(self, params, v0, throttle, t):
        v_max = params.power * throttle / params.drag
        return (v0 - v_max) * exp(-params.drag * t) + v_max

    def solve_linear_motion(self, params, v0, throttle, t):
        ts = np.arange(0., t, 1.)
        v_max = params.power * throttle / params.drag
        exp_ts = np.exp(-params.drag * ts)
        v_profile = (v0 - v_max) * exp_ts + v_max
        x_profile = (v0 - v_max) * (1. - exp_ts) / params.drag + v_max * ts
        return (x_profile, v_profile)

    def get_inv_radius_profile_and_final_coord(self, state0, x_profile):
        x_profile = x_profile + state0.coord.length_offset 
        inv_radius_profile = np.empty(x_profile.shape)

        current_piece = None
        current_piece_position = 0.

        for i in range(0, len(x_profile)):
            if current_piece == None or x_profile[i] > current_piece_position + current_piece_length:
                current_coord = track.beginning_of_next_piece(current_coord) if current_piece != None else state0.coord
                if current_piece != None:
                    current_piece_position += current_piece_length
                current_piece = track.get_lane_piece(current_coord)
                current_piece_length = current_piece.piece_length()
                current_piece_inv_radius = current_piece.initial_inv_radius() 
            inv_radius_profile[i] = current_piece_inv_radius 

        final_coord = TrackCoord(
            current_coord.track_piece_idx,
            current_coord.start_lane_idx,
            current_coord.end_lane_idx,
            x_profile[-1] - current_piece_position
        )
        return (inv_radius_profile, final_coord) 

    def solve_angular_motion(self, params, v0, theta0, omega0, throttle, inv_radius_profile):
        def dydt(t, y):
            v = self.velocity(params, v0, throttle, t)
            inv_radius = inv_radius_profile[clamp(int(t), 0, len(inv_radius_profile) - 1)]
            alpha = self.angular_acceleration(params, v, y[0], y[1], inv_radius)
            return [y[1], alpha]

        def jac(t, y):
            return np.array([
                [0., 1.],
                [params.f_lin * cos(y[0]), params.f_rot]
            ])

        theta_profile = np.empty(inv_radius_profile.shape)
        theta_profile[0] = theta0

        omega_profile = np.empty(inv_radius_profile.shape)
        omega_profile[0] = omega0

        r = ode(dydt, jac)
        r.set_integrator('vode', max_step=5., rtol=0.1) 
        r.set_initial_value((theta0, omega0), 0.)

        for t in np.arange(1., len(inv_radius_profile), 1.):
            r.integrate(t)
            theta_profile[int(t)] = r.y[0]
            omega_profile[int(t)] = r.y[1]

        return (theta_profile, omega_profile)

    # Failed attempt to write a faster solver.
    # def solve_angular_motion_fast(self, params, v0, theta0, omega0, throttle, v_profile, inv_radius_profile):
    #     soln = np.empty((len(inv_radius_profile), 2))
    #     soln[0, :] = (theta0, omega0)

    #     for t in np.arange(1., len(inv_radius_profile), 1.):
    #         (theta_p, omega_p) = soln[t - 1, :]

    #         v = v_profile[int(t)]
    #         inv_radius = inv_radius_profile[int(t)]

    #         cent_torque = params.alpha_cent * v * v * inv_radius 
    #         abs_thresh_cent_torque = max(0., abs(cent_torque) - params.threshold)
    #         thresh_cent_torque = copysign(abs_thresh_cent_torque, cent_torque)


    #         # theta'' = params.f_rot * theta' + params.f_lin * sin(theta) + thresh_cent_torque

    #         # theta'' = params.f_rot * theta' + params.f_lin * (sin(theta_p) + cos(theta_p) * (theta - theta_p)) + thresh_cent_torque

    #         # theta'' = params.f_rot * theta' + params.f_lin * cos(theta_p) * theta + params.f_lin * sin(theta_p) - params.f_lin * cos(theta_p) * theta_p + thresh_cent_torque

    #         # theta'' = a theta' + b theta + c
    #         a = params.f_rot
    #         b = params.f_lin * cos(theta_p)
    #         c = params.f_lin * sin(theta_p) - params.f_lin * cos(theta_p) * theta_p + thresh_cent_torque

    #         d = cmath.sqrt(a*a + 4*b)
    #         
    #         k1 = ((theta_p + c / b) * (a + d) - 2 * omega_p) / (2 * d)
    #         k2 = theta_p + c / b - k1

    #         theta = k1 * cmath.exp(0.5 * (a - d)) + k2 * cmath.exp(0.5 * (a + d)) - c / b

    #         omega = 0.5 * (a - d) * k1 * cmath.exp(0.5 * (a - d)) + 0.5 * (a + d) * cmath.exp(0.5 * (a + d))

    #         print ((theta, omega))

    #         soln[t, :] = (theta, omega)

    #     return soln[:, 0]


    def simulate(self, params, state0, throttle, t):
        (x_profile, v_profile) = self.solve_linear_motion(params, state0.v, throttle, t) 
        (inv_radius_profile, final_coord) = self.get_inv_radius_profile_and_final_coord(state0, x_profile)
        (theta_profile, omega_profile) = self.solve_angular_motion(params, state0.v, state0.theta, state0.omega, throttle, inv_radius_profile)
        state1 = PhysicsModelState(
            final_coord,
            v_profile[-1],
            theta_profile[-1],
            omega_profile[-1]
        )
        return (x_profile, v_profile, theta_profile, state1)

    def learn_params(self, training_data):
        pass

from data.keimola import F1, F2, alpha, threshold, c, d, track
params = PhysicsModelParameters(-F1 * 40, -F2 * 40, alpha, threshold, c, -d)
physics_model = PhysicsModel(track)

# Usage:
# from simulate2 import physics_model, params
# (x, v, theta, final_state) = physics_model.simulate(params, initial_state, throttle, time)
#
# where
#   x, v, theta are numpy arrays
#   initial_state, final_state are PhysicsModelStates
#   throttle is a float (not an array!)
#   time is an integer (not an array!)


# timess = 2000
# 
# tp = np.empty((0))
# 
# from model.track2 import TrackCoord
# 
# state1 = PhysicsModelState(TrackCoord(0, 0, 0, 0), 0, 0, 0)
# for t in np.arange(0.3, 0.6, 0.02): 
#     (x_profile, v_profile, theta_profile, state1) = physics_model.simulate(params, state1, t, 101)
#     tp = np.concatenate((tp, theta_profile[:-1]))
# 
# (x_profile, v_profile, theta_profile, state1) = physics_model.simulate(params, state1, 0.65, 500)
# tp = np.concatenate((tp, theta_profile[:-1]))
# 
# import pylab as pl
# import argparse
# import pickle
# 
# # Load some log data and plot it
# parser = argparse.ArgumentParser()
# parser.add_argument('filename')
# args = parser.parse_args()
# 
# data = pickle.load(open(args.filename))
# data = [x for x in data if 'car_position' in x and x['deflection'] != None and x['angular_speed'] != None and x['angular_acceleration'] != None]
# data = data[0:timess]
# 
# pl.plot(np.arange(len(data)) + 2, [x['deflection'] for x in data], 'r')
# 
# 
# 
# #pl.plot(range(len(x_profile)), x_profile)
# #pl.plot(range(len(v_profile)), v_profile)
# pl.plot(range(len(tp)), tp) 
# #pl.plot(range(len(inv_radius_profile)), inv_radius_profile)
# pl.show()
