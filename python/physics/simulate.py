from __future__ import division
from math import sqrt, sin, cos, pow
import argparse
import pickle
import time


from scipy.integrate import odeint
from scipy.integrate import ode
import numpy as np
import pylab as pl


from model.track2 import CURVED_PIECE, TrackCoord, _make_coord
from data.keimola import track, F1, F2, alpha, radpow, c, d, vpow, threshold, thrust_factor, torque_type
from data.profiles import profile1, constant_profile, step_profile, crash_profile, optimal_profile
d1 = 10.
d2 = 30.


def l1(v, d1, theta, omega):
    return sqrt(pow(v, 2.) + pow(d1 * omega, 2.) - 2. * v * d1 * omega * sin(theta))

def l2(v, d2, theta, omega):
    return sqrt(pow(v, 2.) + pow(d2 * omega, 2.) + 2. * v * d2 * omega * sin(theta))

def torque_old(F1, F2, d1, d2, v, theta, omega):
    l1val = l1(v, d1, theta, omega)
    l2val = l2(v, d2, theta, omega)
    if l1val > 0.:
        front_torque = -F1 / l1(v, d1, theta, omega) * (pow(d1, 2.) * omega - d1 * v * sin(theta))
    else:
        front_torque = 0.
    if l2val > 0.:
        back_torque = -F2 / l2(v, d2, theta, omega) * (pow(d2, 2.) * omega + d2 * v * sin(theta))
    else:
        back_torque = 0.
    return front_torque + back_torque

def torque_new(F1, F2, d1, d2, v, theta, omega):
    linear_torque = -F1 * (d1 + d2) * sin(theta)
    rot_torque = -F2 * (d1 + d2) * omega 

    return linear_torque + rot_torque

def torque(F1, F2, d1, d2, v, theta, omega):
    if torque_type == 'new':
        return torque_new(F1, F2, d1, d2, v, theta, omega)
    elif torque_type == 'old':
        return torque_old(F1, F2, d1, d2, v, theta, omega)

def linear_acceleration(c, d, v, throttle):
    return c * throttle + d * v

def calc_beta(lane_piece):
    if lane_piece.piece_type() == CURVED_PIECE:
        radius = pow(lane_piece.initial_radius(), radpow)

        absbeta = alpha / radius
        if lane_piece.track_piece.angle > 0:
            return absbeta
        else:
            return -absbeta
    else:
        return 0.

def dydt_throttle(t, y, beta, throttle):
    (distance, v, theta, omega) = y

    ang_accel = pow(v, vpow) * beta
    if abs(ang_accel) < threshold:
        ang_accel = 0.
    else:
        if ang_accel > 0:
            ang_accel -= threshold 
        else:
            ang_accel += threshold

    extra_torque = thrust_factor * linear_acceleration(c, d, v, throttle)

    return [v, linear_acceleration(c, d, v, throttle), omega, torque(F1, F2, d1, d2, v, theta, omega) + ang_accel + extra_torque]

def simulate_time(coord0, v0, theta0, omega0, ts, throttle_profile):
    assert(coord0.start_lane_idx == coord0.end_lane_idx)

    soln = np.array([[coord0.length_offset, v0, theta0, omega0, coord0.track_piece_idx]])

    r = ode(dydt_throttle)
    r.set_initial_value((coord0.length_offset, v0, theta0, omega0), ts[0]) 
    current_coord = coord0
    current_piece = track.get_lane_piece(current_coord)
    current_piece_distance = 0.
    current_piece_length = current_piece.piece_length()

    for t_idx in range(1, len(ts)):
        r.set_f_params(calc_beta(current_piece), throttle_profile[t_idx])
        r.integrate(ts[t_idx])
        (distance, v, theta, omega) = r.y

        soln = np.append(soln, np.array([np.append(r.y, current_coord.track_piece_idx)]), axis=0)

        while distance - current_piece_distance > current_piece_length:
            current_piece_distance += current_piece_length
            current_coord = track.beginning_of_next_piece(current_coord)
            current_piece = track.get_lane_piece(current_coord)
            current_piece_length = current_piece.piece_length()

    return soln

def simulate_dist(coord0, v0, theta0, omega0, throttle_dist_profile):
    assert(coord0.start_lane_idx == coord0.end_lane_idx)

    soln = np.array([[coord0.length_offset, v0, theta0, omega0, coord0.track_piece_idx]])

    def dydt(t, y, beta):
        (distance, v, theta, omega) = y
        throttle = throttle_dist_profile[min(int(distance), len(throttle_dist_profile) - 1)]
        return dydt_throttle(t, y, beta, throttle)

    r = ode(dydt)
    r.set_initial_value((coord0.length_offset, v0, theta0, omega0), 0.) 
    current_coord = coord0
    current_piece = track.get_lane_piece(current_coord)
    current_piece_distance = 0.
    current_piece_length = current_piece.piece_length()
    r.set_f_params(calc_beta(current_piece))

    t_idx = 0
    while True:
        r.set_f_params(calc_beta(current_piece))
        r.integrate(r.t + 1.)
        (distance, v, theta, omega) = r.y

        if int(distance) + 10 > len(throttle_dist_profile):
            break

        soln = np.append(soln, np.array([np.append(r.y, current_coord.track_piece_idx)]), axis=0)

        while distance - current_piece_distance > current_piece_length:
            current_piece_distance += current_piece_length
            current_coord = track.beginning_of_next_piece(current_coord)
            current_piece = track.get_lane_piece(current_coord)
            current_piece_length = current_piece.piece_length()

        t_idx += 1

        if t_idx > 2000:
            return None

    return soln

if __name__ == "__main__":
    # Note that you need to do "export PYTHONPATH=." to run this.
    # Then run "python physics/simulate.py" from the root python directory.

    start_idx = 0

    # Load some log data and plot it
    parser = argparse.ArgumentParser()
    parser.add_argument('filename')
    args = parser.parse_args()

    data = pickle.load(open(args.filename))
    data = [x for x in data if 'car_position' in x and x['deflection'] != None and x['angular_speed'] != None and x['angular_acceleration'] != None]
    data = data[start_idx:]

    pl.plot(np.arange(len(data)) + 2, [x['deflection'] for x in data], 'r')
    #pl.plot(np.arange(len(data)) + 2, [x['car_position']['piecePosition']['pieceIndex']/40 for x in data], 'r')
    #pl.plot(np.arange(len(data)) + 2, [x['angular_speed'] for x in data], 'r')
    #pl.plot(np.arange(len(data)) + 2, [x['speed'] for x in data], 'r')

    start_point = data[0] 
    coord0 = _make_coord(start_point['car_position']['piecePosition'])
    v0 = start_point['speed']
    theta0 = start_point['deflection']
    omega0 = start_point['angular_speed']

    # Simulate and plot a run around the track
    ts = np.arange(0, 2000, 1)
    #soln = simulate_time(coord0, v0, theta0, omega0, ts, optimal_profile) 
    soln = simulate_time(TrackCoord(0, 0, 0, 0), 0, 0, 0, ts, step_profile) 


    #soln = simulate_dist(TrackCoord(0, 0, 0, 0), 0, 0, 0, optimal_profile) 
    #pl.plot(range(len(soln[:, 2])), soln[:, 2], 'b')
    #pl.plot(ts, soln[:, 4] / 40, 'b')
    pl.plot(ts, soln[:, 2], 'b')

    #soln1 = simulate_time(TrackCoord(0, 0, 0, 4), 0.13, 0, 0, np.arange(0., 100.), np.ones(100) * 0.39)
    #soln2 = simulate_time(TrackCoord(0, 0, 0, 4), 0.13, 0, 0, np.arange(0., 100.), np.ones(100) * 0.8)
    #soln3 = simulate_time(TrackCoord(0, 0, 0, 4), 0.13, 0, 0, np.arange(0., 100.), np.ones(100) * 1.)

    #pl.plot(range(len(soln1)), soln1[:, 0], 'r')
    #pl.plot(range(len(soln2)), soln2[:, 0], 'g')
    #pl.plot(range(len(soln3)), soln3[:, 0], 'b')

    # Show the plots
    pl.show()
