from model.track2 import _make_track

track_json = {u'pieces': [{u'length': 100.0}, {u'length': 100.0}, {u'length': 100.0}, {u'length': 100.0, u'switch': True}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 22.5, u'switch': True, u'radius': 200}, {u'length': 100.0}, {u'length': 100.0}, {u'angle': -22.5, u'radius': 200}, {u'length': 100.0}, {u'length': 100.0, u'switch': True}, {u'angle': -45.0, u'radius': 100}, {u'angle': -45.0, u'radius': 100}, {u'angle': -45.0, u'radius': 100}, {u'angle': -45.0, u'radius': 100}, {u'length': 100.0, u'switch': True}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 22.5, u'radius': 200}, {u'angle': -22.5, u'radius': 200}, {u'length': 100.0, u'switch': True}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'length': 62.0}, {u'angle': -45.0, u'switch': True, u'radius': 100}, {u'angle': -45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'length': 100.0, u'switch': True}, {u'length': 100.0}, {u'length': 100.0}, {u'length': 100.0}, {u'length': 90.0}], u'lanes': [{u'index': 0, u'distanceFromCenter': -10}, {u'index': 1, u'distanceFromCenter': 10}], u'id': u'keimola', u'startingPoint': {u'position': {u'y': -44.0, u'x': -300.0}, u'angle': 90.0}, u'name': u'Keimola'}

track = _make_track(track_json)

# Values calculated by minimzing penalty function using general purpose
# function minimizer. (To be committed later). 

# # Front wheel friction
# F1 = 7.90567869e-04
# 
# # Back wheel friction
# F2 = 5.40546566e-04
# 
# # Mysterious angular acceleration term = alpha * v^vpow / r^rpow
# alpha = 1.04035006e-05
# radpow = 3.63536217e+00
# vpow = 1.21306899e+01 
# 
# threshold = 0
# threshold2 = 0

# Linear acceleration = c * throttle + d * velocity
c = 0.2
d = -0.02

#F1 = 0.00079663 
#F2 = 0.00054199
#alpha = 0.03564785
#threshold = 0.01080012

# F1 = 0.00071778
# F2 = 0.0004809
# alpha = 0.04037125
# threshold = 0.01292235

if True:
    torque_type = 'new'
    ## New torque parameters
    F1 = 2.31720554e-04
    F2 = 2.53495049e-03
    alpha = 5.18675478e-02
    thrust_factor = -2.01211089e-09
    vpow = 2. 
    radpow = 1.
    threshold = 1.63756068e-02
else:
    torque_type = 'old'
    #old torque parameters
    F1 = 9.90485208e-04
    F2 = 6.27990488e-04
    alpha = 4.85211970e-02
    thrust_factor = 1.88160140e-02
    vpow = 2. 
    radpow = 1.
    threshold = 1.52951205e-02
