import numpy as np
from functools import partial
import random
import socket

from bots.throttlebot import ThrottleBot

host = "testserver.helloworldopen.com"
port = 8091
name = "Ducky"
key = "bHhTMk7zkBKLBA"


def run_experiment(throttleFn):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))
    bot = ThrottleBot(s, name, key, throttleFn)
    bot.run()


def constantThrottleFn(throttle):
    return lambda x, y: throttle


def randomThrottleFn(x, y, throttle, variation):
    return throttle + (((random.random() - 0.5) * 2) * variation * throttle)

for throttle in np.arange(0.6, 0.68, 0.005):
    run_experiment(constantThrottleFn(throttle))

for variation in np.arange(0.1, 0.3, 0.05):
    for x in range(5):
        run_experiment(partial(randomThrottleFn, throttle=0.65, variation=variation))
